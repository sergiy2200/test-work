FROM node:16-alpine3.14 
WORKDIR /project
COPY . .
RUN npm install 
CMD npm start
